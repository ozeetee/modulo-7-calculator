package agoto.io.modulo7calculator;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by gaurav on 17/07/16.
 */
public class ButtonsAdapter extends RecyclerView.Adapter<ButtonsAdapter.ButtonViewHolder> {

    private OnButtonClickListener clickListener;


    public ButtonsAdapter(OnButtonClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public ButtonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cal_button, parent, false);
        final ButtonViewHolder vh = new ButtonViewHolder(view);
        vh.getBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txt = ((Button) view).getText().toString();
                if(Utils.isEmpty(txt)) return;
                if(Utils.isOperator(txt)){
                    clickListener.onOperatorClicked(Utils.operatorFromString(txt));
                }else if(Utils.isOperand(txt)){
                    clickListener.onOperandClicked(Integer.parseInt(txt));
                }
            }
        });
        return new ButtonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ButtonViewHolder holder, int pos) {
        String txt = CC.buttonLabels[pos];
        holder.getBtn().setText(txt);
    }

    @Override
    public int getItemCount() {
        return CC.buttonLabels.length;
    }

    static class ButtonViewHolder extends RecyclerView.ViewHolder {

        private Button btn;

        public ButtonViewHolder(View itemView) {
            super(itemView);
            btn = (Button)itemView.findViewById(R.id.btn);
        }

        public Button getBtn(){
            return btn;
        }
    }

    interface OnButtonClickListener{
        void onOperandClicked(int operand);
        void onOperatorClicked(CC.OPERATORS operator);
    }

}
