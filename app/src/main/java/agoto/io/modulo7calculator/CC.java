package agoto.io.modulo7calculator;

/**
 * Created by gaurav on 17/07/16.
 *
 * Calculator constants
 *
 */
public interface CC {

    enum OPERATORS{

        MUL ("x"),
        SUB ("-"),
        ADD ("+"),
        DEL ("DEL"),
        EQU ("=");

        public final String label;

        OPERATORS(String label){
            this.label = label;
        }

    }

    String[] buttonLabels = {"1", "2", "3", OPERATORS.MUL.label, "4", "5", "6", OPERATORS.SUB.label, OPERATORS.DEL.label, "0", OPERATORS.EQU.label, OPERATORS.ADD.label};
}
