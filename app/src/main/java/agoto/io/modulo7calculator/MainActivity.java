package agoto.io.modulo7calculator;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


/**
 * Created by gaurav on 17/07/16.
 *
 * Handled cases :
 * 1. if evaluated and press number : clear previous result
 * 2. if evaluated and press operator : don't clear previous result. just append
 *
 */
public class MainActivity extends AppCompatActivity implements ButtonsAdapter.OnButtonClickListener {

    private static final String CALTEXT = "CALTEXT";
    private static final int LIMIT = 20; //Allowing 20 characters only

    private TextView mTextView;
    private TextView mErrorTextView;

    private RecyclerView mButtons;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    //C JNI interface for evaluating expression modulo 7
    private CInterfaceJni jni;


    //Buffer drawable and string for later use
    private Drawable normalTVDrawable;
    private Drawable errorTVDrawable;
    private String expressionLimitReached;
    private String invalidExpression;

    //State variables
    private StringBuilder stringBuilder;

    private boolean evaluated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        jni = new CInterfaceJni();

        normalTVDrawable = getResources().getDrawable(R.drawable.drawable_tv);
        errorTVDrawable = getResources().getDrawable(R.drawable.drawable_tv_error);

        expressionLimitReached = getResources().getString(R.string.expression_limit);
        invalidExpression = getResources().getString(R.string.invalid_expression);

        mTextView = (TextView) findViewById(R.id.tv_cal_result);
        mErrorTextView = (TextView) findViewById(R.id.tv_input_error);
        mButtons = (RecyclerView) findViewById(R.id.rv_buttons);
        if(savedInstanceState != null && savedInstanceState.get(CALTEXT) != null){
            stringBuilder = (StringBuilder) savedInstanceState.getSerializable(CALTEXT);
        }

        if(stringBuilder == null) stringBuilder = new StringBuilder();
        initButtons();
        refreshText();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CALTEXT,stringBuilder);
    }

    private void initButtons() {
        // use a linear layout manager
        mLayoutManager = new GridLayoutManager(this,4);
        mButtons.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new ButtonsAdapter(this);
        mButtons.setAdapter(mAdapter);
    }


    @Override
    public void onOperandClicked(int operand) {
        if(evaluated){
            evaluated = false;
            stringBuilder = new StringBuilder();
        }

        //Check before appending if the limit is reached
        if(stringBuilder.length() >= LIMIT){
            mErrorTextView.setText(expressionLimitReached);
            mErrorTextView.setVisibility(View.VISIBLE);
            return;
        }
        stringBuilder.append(operand);
        refreshText();
    }

    @Override
    public void onOperatorClicked(CC.OPERATORS operator) {
        if(stringBuilder.length() == 0){
            //Operator can only be first if its - or +

            if(operator == CC.OPERATORS.ADD || operator == CC.OPERATORS.SUB){
                stringBuilder.append(operator.label);
                refreshText();
                evaluated = false;
            }

            return;
        }

        //Handle Del and equals
        if(operator == CC.OPERATORS.DEL){
            handleDel();
            return;
        }

        //Handle Equ
        if(operator == CC.OPERATORS.EQU){
            handleEQ();
            return;
        }

        //Check if we already have an operator as a last element
        char c = stringBuilder.charAt(stringBuilder.length() - 1);

        if(Utils.isOperator(c)) return; //Can't have 2 operators together

        //Check before appending if the limit is reached
        if(stringBuilder.length() >= LIMIT){
            mErrorTextView.setText(expressionLimitReached);
            mErrorTextView.setVisibility(View.VISIBLE);
            return;
        }

        stringBuilder.append(operator.label);

        refreshText();
        evaluated = false;
    }

    private void handleEQ() {
        //Validate the expression first before sending it to C Code
        if(!Utils.isValidExpression(stringBuilder)){
            mErrorTextView.setText(invalidExpression);
            mErrorTextView.setVisibility(View.VISIBLE);
            Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
            mTextView.startAnimation(shake);
            mTextView.setBackground(errorTVDrawable);
            evaluated = false;
            return;
        }

        StringBuilder formattedExpression = Utils.formatExpression(stringBuilder);
        int txt = jni.evaluate(formattedExpression.toString());
        stringBuilder = new StringBuilder(String.valueOf(txt));
        refreshText();
        evaluated = true;
    }


    private void handleDel(){
        if(stringBuilder.length() == 0 ) return;
        evaluated = false;
        stringBuilder.deleteCharAt(stringBuilder.length() -1);
        refreshText();
    }

    private void refreshText() {
        mTextView.setBackground(normalTVDrawable);
        mErrorTextView.setVisibility(View.INVISIBLE);
        mTextView.setText(stringBuilder.toString());
    }

}
