package agoto.io.modulo7calculator;

import android.support.annotation.Nullable;

/**
 * Created by gaurav on 17/07/16.
 */
public class Utils {

    public static boolean isEmpty(String s){
        return  (s == null || "".equals(s));
    }

    public static boolean isOperator(String s){
        if(isEmpty(s)) return false;
        for (CC.OPERATORS op : CC.OPERATORS.values()){
            if(op.label.equalsIgnoreCase(s)) return true;
        }
        return false;
    }

    public static boolean isOperator(char c){
        return isOperator(String.valueOf(c));
    }


    public static boolean isOperand(String s){
        if(isEmpty(s)) return false;
        try {
            Integer.parseInt(s);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    @Nullable
    public static CC.OPERATORS operatorFromString(String s){
        for (CC.OPERATORS op : CC.OPERATORS.values()){
            if(op.label.equalsIgnoreCase(s)) return op;
        }
        return null;
    }


    public static boolean isValidExpression(StringBuilder builder){
        //TODO : Write a better validation code with regular expression
        // Right now we can live with just validating the last character
        char lastChar = builder.charAt(builder.length()-1);
        return Character.isDigit(lastChar);
    }

    /**
     * Format the string to send it to C code for evaluation
     * @param builder
     * @return
     */
    public static StringBuilder formatExpression(StringBuilder builder){
        if(builder == null || builder.length() == 0) return builder;
        char fistChar = builder.charAt(0);
        if(! Character.isDigit(fistChar)){
            builder.insert(0,'0');
        }
        return builder;
    }

}
