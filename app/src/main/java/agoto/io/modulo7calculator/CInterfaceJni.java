package agoto.io.modulo7calculator;

/**
 * Created by gaurav on 17/07/16.
 */
public class CInterfaceJni {

    public native int evaluate(String expression);

    static {
        System.loadLibrary("io.agoto.evaluator");
    }

}
