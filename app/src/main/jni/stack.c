#include "Stack.h"

void initOperandStack(OperandStack *s)
{
    s->top=-1;
}

int emptyOperandStack(OperandStack *s)
{
    if(s->top==-1)
        return(1);

    return(0);
}

int fullOperandStack(OperandStack *s)
{
    if(s->top==MAX-1)
        return(1);

    return(0);
}

void pushOperandStack(OperandStack *s,int x)
{
    s->top=s->top+1;
    s->data[s->top]=x;
}

int popOperandStack(OperandStack *s)
{
    int x;
    x=s->data[s->top];
    s->top=s->top-1;
    return(x);
}

int topOperandStack(OperandStack *p)
{
    return (p->data[p->top]);
}




void initOperatorStack(OperatorStack *s)
{
    s->top=-1;
}

int emptyOperatorStack(OperatorStack *s)
{
    if(s->top==-1)
        return(1);

    return(0);
}

int fullOperatorStack(OperatorStack *s)
{
    if(s->top==MAX-1)
        return(1);

    return(0);
}

void pushOperatorStack(OperatorStack *s,char x)
{
    s->top=s->top+1;
    s->data[s->top]=x;
}

char popOperatorStack(OperatorStack *s)
{
    int x;
    x=s->data[s->top];
    s->top=s->top-1;
    return(x);
}

char topOperatorStack(OperatorStack *p)
{
    if(p->top==-1) return('0'); //Return 0 if operator stack is empty
    return (p->data[p->top]);
}