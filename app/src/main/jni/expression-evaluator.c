#include <jni.h>
#include "evaluator.h"

#include <android/log.h>

#define APPNAME "MOD7CAL"

jint
Java_agoto_io_modulo7calculator_CInterfaceJni_evaluate( JNIEnv* env,jobject thiz, jstring javaString )
{

    const char *nativeString = (*env)->GetStringUTFChars(env, javaString, 0);

    int result = evaluate(nativeString);
    int retVal = modulo7(result);
    __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "Input expression %s",nativeString);

    (*env)->ReleaseStringUTFChars(env, javaString, nativeString);
    return retVal;


}




// Test case 1
//  1 + 2 * 4 * 5 + 6 - 3 * 5
//    infix[0] = '1';
//    infix[1] = '+';
//    infix[2] = '2';
//    infix[3] = 'x';
//    infix[4] = '4';
//    infix[5] = 'x';
//    infix[6] = '5';
//    infix[7] = '+';
//    infix[8] = '6';
//    infix[9] = '-';
//    infix[10] = '3';
//    infix[11] = 'x';
//    infix[12] = '5';
//    infix[13] = '\0';