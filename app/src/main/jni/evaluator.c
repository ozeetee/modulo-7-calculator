#include <stdio.h>
#include "evaluator.h"


int evaluate(char * infix){
    OperatorStack operatorStack;
    OperandStack operandStack;

    initOperatorStack(&operatorStack);
    initOperandStack(&operandStack);

    int i = 0;
    char token;

    while (infix[i]!='\0'){
        token=infix[i];
        if(isdigit(token)){
            //There is a number here... Parse and push into stack
            int temp = parseInt(infix, &i);
            pushOperandStack(&operandStack,temp);
        }else{
            if(precedence(token) > precedence(topOperatorStack(&operatorStack))){
                pushOperatorStack(&operatorStack,token);
                i++;
            }else {
                while (precedence(token) <= precedence(topOperatorStack(&operatorStack))){
                    int operand1 = popOperandStack(&operandStack);
                    int operand2 = popOperandStack(&operandStack);

                    char operator = popOperatorStack(&operatorStack);
                    int result = evalTwoOperand(operand1,operand2, operator);
                    pushOperandStack(&operandStack,result);
                }

                pushOperatorStack(&operatorStack,token);
                i++;
            }
        }
    }
    while(!emptyOperatorStack(&operatorStack)){
        int operand1 = popOperandStack(&operandStack);
        int operand2 = popOperandStack(&operandStack);
        char operator = popOperatorStack(&operatorStack);
        int result = evalTwoOperand(operand1,operand2, operator);
        pushOperandStack(&operandStack,result);
    }

    return topOperandStack(&operandStack);
}


int parseInt(char * infix, int *i){
    int count = 0,retVal,k, j = *i;
    char token = infix[j];

    while (isdigit(token)){
        count ++;
        token = infix[++j];
    }

    char strNum[count + 1];

    for (k = 0; k < count; k++) {
        strNum[k] = infix[*i + k];
    }
    strNum[count] = '\0';

    sscanf(strNum, "%d", &retVal);

    *i = *i + count;
    return retVal;
}

int evalTwoOperand(int operand1, int operand2,char operator){
    if(operator == '+'){
        return operand2 + operand1;
    }
    if(operator == 'x'){
        return operand2 * operand1;
    }

    if(operator == '-'){
        return operand2 - operand1;
    }
    return 0;
}


//We only need to check for precedence of + - and x
int precedence(char x){
    if(x =='+'|| x== '-')
        return 0;
    if(x == 'x')
        return 1;
    return -1;
}

int modulo7(int x){
    return x > 0 ? x % 7 : x + 7 * ((7 - x - 1) / 7);
}
