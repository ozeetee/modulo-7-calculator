#define MAX 100

typedef struct OperandStack
{
    int data[MAX];
    int top;
}OperandStack;

typedef struct OperatorStack
{
    char data[MAX];
    int top;
}OperatorStack;


void initOperandStack(OperandStack *);
int emptyOperandStack(OperandStack *);
int fullOperandStack(OperandStack *);
int popOperandStack(OperandStack *);
void pushOperandStack(OperandStack *, int);
int topOperandStack(OperandStack *);

void initOperatorStack(OperatorStack *);
int emptyOperatorStack(OperatorStack *);
int fullOperatorStack(OperatorStack *);
char popOperatorStack(OperatorStack *);
void pushOperatorStack(OperatorStack *, char);
char topOperatorStack(OperatorStack *);



