package agoto.io.modulo7calculator;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by gaurav on 18/07/16.
 */
public class UtilsTest {

    @Test
    public void testIsEmptyNull() throws Exception {
        assertTrue(Utils.isEmpty(null));
    }

    @Test
    public void testIsEmptyQuotes() throws Exception {
        assertTrue(Utils.isEmpty(""));
    }

    @Test
    public void testNotIsEmptyNot() throws Exception {
        assertFalse(Utils.isEmpty("gt"));
    }

    @Test
    public void testIsOperator() throws Exception {
        assertTrue(Utils.isOperator('-'));
        assertTrue(Utils.isOperator('+'));
        assertTrue(Utils.isOperator('x'));
    }

    @Test
    public void testIsNotOperator() throws Exception {
        assertFalse(Utils.isOperator('('));
        assertFalse(Utils.isOperator(')'));
        assertFalse(Utils.isOperator('*'));
    }

    @Test
    public void testIsOperand() throws Exception {
        assertTrue(Utils.isOperand("1"));
        assertTrue(Utils.isOperand("2"));
        assertTrue(Utils.isOperand("0"));
    }

    @Test
    public void testIsNotOperand() throws Exception {
        assertFalse(Utils.isOperand("+"));
        assertFalse(Utils.isOperand("-"));
        assertFalse(Utils.isOperand("x"));
    }

    @Test
    public void testOperandFromString() throws Exception{
        CC.OPERATORS operator = Utils.operatorFromString("x");
        assertEquals("x",operator.label);
    }

    @Test
    public void testFormatString() throws Exception{
        StringBuilder actual = Utils.formatExpression(new StringBuilder("+90"));
        assertEquals("0+90",actual.toString());
    }



}
