package agoto.io.modulo7calculator;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by gaurav on 18/07/16.
 * UI Automation test for testing the UI Flow
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class UIAutomationTests {

    @Rule
    public ActivityTestRule<MainActivity> mMainActivityTestRule =  new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testButtonsWorkingCorrectly(){
        onView(withText("1")).perform(click());
        onView(withText("2")).perform(click());
        onView(withText("+")).perform(click());
        onView(withText("1")).perform(click());
        onView(withId(R.id.tv_cal_result)).check(matches(withText("12+1")));
    }


    @Test
    public void testInvalidExpression(){
        //Error message should be visible
        onView(withText("1")).perform(click());
        onView(withText("2")).perform(click());
        onView(withText("x")).perform(click());
        onView(withText("=")).perform(click());
        onView(withId(R.id.tv_input_error)).check(matches(isDisplayed()));
    }

    @Test
    public void testValidEvaluation(){
        //Error message should be visible
        onView(withText("1")).perform(click());
        onView(withText("2")).perform(click());
        onView(withText("x")).perform(click());
        onView(withText("3")).perform(click());
        onView(withText("=")).perform(click());
        onView(withId(R.id.tv_cal_result)).check(matches(withText("1")));
    }

    @Test
    public void testDelWorkingCorrectly(){
        //Error message should be visible
        onView(withText("1")).perform(click());
        onView(withText("2")).perform(click());
        onView(withText("x")).perform(click());
        onView(withText("3")).perform(click());
        onView(withText("DEL")).perform(click());
        onView(withId(R.id.tv_cal_result)).check(matches(withText("12x")));
    }


    @Test
    public void testAddWorkingCorrectly(){
        //Error message should be visible
        onView(withText("1")).perform(click());
        onView(withText("+")).perform(click());
        onView(withText("6")).perform(click());
        onView(withText("=")).perform(click());
        onView(withId(R.id.tv_cal_result)).check(matches(withText("0")));
    }

    @Test
    public void testSubWorkingCorrectly(){
        //Error message should be visible
        onView(withText("4")).perform(click());
        onView(withText("+")).perform(click());
        onView(withText("3")).perform(click());
        onView(withText("=")).perform(click());
        onView(withId(R.id.tv_cal_result)).check(matches(withText("0")));
    }

    @Test
    public void testMulWorkingCorrectly(){
        //Error message should be visible
        onView(withText("5")).perform(click());
        onView(withText("x")).perform(click());
        onView(withText("2")).perform(click());
        onView(withText("=")).perform(click());
        onView(withId(R.id.tv_cal_result)).check(matches(withText("3")));
    }

}
