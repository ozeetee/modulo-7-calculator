# README #

Implementation of modulo 7 calculator.

### Implementation ###

#### UI ####
* UI part is implemented using **Java** in Android SDK.
* For laying out buttons Recycler view is used.

#### Calculations & expression evaluation ####
* All calculations and expression evaluation is done in **C** code
* Expression is evaluated using double stacks. One for operand and one for operator.
* UI layers integrated to C code using **JNI**

### How to run locally ? ###

* Clone repository using git
* Open project in Android Studio
* Run the project


### Test cases ###

* UI Automation for calculator. src/androidTest/java/agoto.io.modulo7calculator/UIAutomationTests
* Run using Android Studio
* JUnit test cases src/test

### TODO : ###

* Create Test cases for C code.
* Handling large integers in expressions. (MAX INT, MIN INT) 
* Should use long for supporting large numbers.
* Better validations using regular expressions.
* Write more tests for testing max int, min int values
* Refactor C code a bit

### Created By ###

* Gaurav Tiwari (ozeetee)